FROM centos:latest

#Adding Apache web server
RUN yum -y install httpd || yum update httpd
RUN yum -y install php || yum update php

RUN mkdir -p /var/log/httpd/

#Create the logging directories
RUN echo "IncludeOptional /*.conf" > /etc/httpd/conf.d/apache.conf

#Setting apache as an entrypoint and running it in the background
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
EXPOSE 80


